﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;
public class WWWManager : MonoBehaviour {

    private const string URL = "https://script.google.com/macros/s/AKfycbzT_6L2qO14szvdp0iUzp7njTBPWJeNW3bhZ63FlosnA8IYiVL6/exec";

    [HideInInspector]
    public bool isLoading = false;

    void Start() { }

    //DataManagerの部分は、シングルトンにするクラス名を指定して下さい。
    static public WWWManager instance;
    void Awake ()
    {
        if (instance == null) {

            instance = this;
            DontDestroyOnLoad (gameObject);
        }
        else {

            Destroy (gameObject);
        }

    }


    public WWW GET(string url)
    {
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
        return www;
    }

    public WWW GET_D(string method_name, string e1 = "null", string e2 = "null", string e3 = "null", string e4 = "null", string e5 = "null")
    {
        isLoading = true;
        
        string url = "";

        url += "method_name=" + method_name + "&";
        url += "e1=" + e1 + "&";
        url += "e2=" + e2 + "&";
        url += "e3=" + e3 + "&";
        url += "e4=" + e4 + "&";
        url += "e5=" + e5 + "&";
        url += "api_pass=" + "iAt4Du4rSGH4tyDfve5fsHhS";
        url = URL + "?" + url;
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
        return www;
    }


    public WWW POST(string url, Dictionary<string, string> post)
    {
        WWWForm form = new WWWForm();
        foreach (KeyValuePair<String, String> post_arg in post)
        {
            form.AddField(post_arg.Key, post_arg.Value);
        }
        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequest(www));
        return www;
    }

    public WWW POST_D(string method_name = "", string e1 = "", string e2 = "", string e3 = "", string e4 = "", string e5 = "")
    {
        Dictionary<string, string> postJson = new Dictionary<string, string>();
        postJson.Add("method_name",method_name);
        postJson.Add("e1",e1);
        postJson.Add("e2",e2);
        postJson.Add("e3",e3);
        postJson.Add("e4",e4);
        postJson.Add("e5",e5);
        postJson.Add("api_pass","iAt4Du4rSGH4tyDfve5fsHhS");

        WWWForm form = new WWWForm();
        foreach (KeyValuePair<String, String> post_arg in postJson)
        {
            form.AddField(post_arg.Key, post_arg.Value);
        }
        form.headers["content-type"] = "application/json";

        WWW www = new WWW(URL, form);

        StartCoroutine(WaitForRequest(www));
        return www;
    }

    private IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
        }
        else
        {
            Debug.LogError("WWW Error: " + www.error + www.text);
        }
        isLoading = false;

    }

    public IEnumerator Login(){
        print("login start");
        WWW www = GET_D("sign_in","100002");

        yield return www;
        print("login end");
        var dict = Json.Deserialize(www.text) as Dictionary<string,object>;

        UserManager.instance.currentUser = JsonUtility.FromJson<User>(www.text);
    }


}