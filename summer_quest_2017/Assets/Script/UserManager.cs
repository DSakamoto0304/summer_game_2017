﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;



public class UserManager : MonoBehaviour {

    public User currentUser;


    static public UserManager instance;
    void Awake ()
    {
        if (instance == null) {

            instance = this;
            DontDestroyOnLoad (gameObject);
        }
        else {
            Destroy (gameObject);
        }
    }
    public IEnumerator Login(){
        WWW www = WWWManager.instance.GET_D("sign_in","100002");
        yield return www;
        var dict = Json.Deserialize(www.text) as Dictionary<string,object>;
        instance.currentUser = JsonUtility.FromJson<User>(www.text);
    }

    public IEnumerator Gacha(){
        WWW www = WWWManager.instance.GET_D("gacha","100002","1");
        yield return www;
        var dict = Json.Deserialize(www.text) as Dictionary<string,object>;
        instance.currentUser = JsonUtility.FromJson<User>(www.text);
    }
}

public class User{
    public int user_id;
    public string user_name;
    public int coin;
    public int stage_progress;
    public string create_time;
    public string update_time;
}
